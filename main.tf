provider "aws"{
 profile = var.username
 region=var.region
}

resource "aws_instance" "VM" {

  count = var.icount
  ami           = var.ami
  instance_type = var.type

  tags ={
    Name = "${format("VM-%03d", count.index + 1)}"
  }

}

output "ec2_public_ip" {
  value = ["${aws_instance.VM.*.public_ip}"]

  }

  output "ec2_private_ip" {
    value = ["${aws_instance.VM.*.private_ip}"]

    }
